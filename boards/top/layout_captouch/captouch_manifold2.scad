r = 8.5;         //radius of the captouch area
segments = 3;    //how many parts in a circle, 3 for badge23
rows = 2;        //how many 'leaves' in a circle
distance = 0.5;    //in mm between segments
stem_angle = 10; //width of the 'stem' in deg on each side
extra_stem = 1;
extra_shrink = 33;
deg_step = 1;

segment_angle = (360 / segments);
extra_rows = 100;


function cartesian(r,phi) = [r*sin(phi),r*cos(phi)];
function r_row(row,a) = row * r/(rows);                         //base radius of row
function row_offset(a)= (a/segment_angle)*1 * r/rows;        //thickness offset dependent on angle
function row_offset_inv(a)= (1-(a/segment_angle)*1) * r/rows; //and the other way around

function seg_angle(row) = (segment_angle-stem_angle(row));
function stem_angle(row) = row<=rows+extra_stem? stem_angle : round(stem_angle+extra_shrink*(row-rows)/rows);

//All points for a segment
points = [ 
    //top (y+) sector, start at the origin, go up row by row counterclockwise
    //We are doing nested list comprehension here
    //'each' unrolls the inner lists so we have a single flat lists of Vec2D coordinate pairs
    for (row = [1:rows+extra_rows]) each [
        for (a = [stem_angle(row):deg_step:seg_angle(row)]) cartesian(r_row(row,a)-row_offset_inv(a),a), //right leave outwards
        for (a = [seg_angle(row):-deg_step:stem_angle(row+1)]) cartesian(r_row(row,a),a) //right leave inwards
    ],
    
    [3*r*sin(60),3*r*cos(60)],
    [r,1.9*r],
    [0,2*r],
    [-r,1.9*r],
    [-3*r*sin(60),3*r*cos(60)],    
    
    if(false) for (extra=[0:-1:0]) each [
        for (a = [segment_angle/2:1:segment_angle*0.8]) cartesian(r_row(rows+extra,a)+1*row_offset_inv(a),-a),
        for (a = [segment_angle*0.8:-1:stem_angle]) cartesian(r_row(rows+extra,a),-a),
    ],
    
    //We are at the top, go down again
    if (true) for (row = [rows+extra_rows:-1:1]) each [
        for (a = [stem_angle(row):deg_step:seg_angle(row)]) cartesian(r_row(row,a)-row_offset(a),-a), //left leave inwards
        for (a = [seg_angle(row):-deg_step:stem_angle(row-1)]) cartesian(r_row(row-1,a),-a) //left leave outwards
    //should be back at (0,0)
    ]
];
    
module subpetal(){
    
    offset(r=-distance/2) polygon(points);
}
        
rotate([0,0,180]) if(true){
    color("yellow") rotate([0,0,0]) subpetal();
    color("red") rotate([0,0,120]) subpetal();
    color("green") rotate([0,0,240]) subpetal();
}

//for (i=[0:segments]) color("green") rotate([0,0,i*segment_angle]) subpetal();
