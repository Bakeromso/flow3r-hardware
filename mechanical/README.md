# Mechanical Design

This folder contains a CAD model of all the components of flow3r.

![flow3r CAD model](../pics/flow3r_cad.jpg)

## Software requirements

We use [FreeCAD](https://www.freecad.org/downloads.php) for the mechanical design.

To display and interact with our design files properly you will need to install the following FreeCAD workbenches via ```Tools -> Addon Manager```

* [Assembly 4](https://github.com/Zolko-123/FreeCAD_Assembly4/) (required)
* [SheetMetal](https://github.com/shaise/FreeCAD_SheetMetal/) (required for the battery cover)
* [KicadStepUp](https://raw.githubusercontent.com/easyw/kicadStepUpMod/) (recommended, required to interact with EDA pcb files)
* [Curves](https://github.com/tomate44/CurvesWB) (recommended. used for the cable paths)


If you are not familiar with these workbenches some basic user guides can be found here:
* [Assembly 4 user instructions](https://github.com/Zolko-123/FreeCAD_Assembly4/blob/master/INSTRUCTIONS.md)
* [FreeCAD SheetMetal Workbench documentation](https://wiki.freecad.org/SheetMetal_Workbench)
* [KicadStepUp Cheat Sheet](https://raw.githubusercontent.com/easyw/kicadStepUpMod/master/demo/kicadStepUp-cheat-sheet.pdf)
* [FreeCAD Curces Workbench codumentation](https://wiki.freecad.org/Curves_Workbench)

## File Structure

### Full assembly
The flow3rs full assembly can be found in **0_flow3r.FCStd**. Here all the various subunits are put together using Assembly4 to get a full mechanical model of the badge. Opening this file in FreeCAD will preload all referenced subunit files in FreeCADs model tree.

### Master sketches

**master_sketches.FCStd** contains sketches of globally relevant features like pcb outlines, screw and led positions, clearance zones etc. We use these with KicadStepup to create edgecuts or to position components like LEDs or screw holes in EDA. **master_sketches.FCStd** also contains a spreadsheet with dimensions used in different subunits of the badge.
The master sketches can be used as a guide on how to construct the outline of the flow3r badge from arcs.

### Spacer model
**spacer.FCStd** contains the model of the spacer. We offer variants of the spacer with slightly different design features. Currently a **default** and a **qwiic** friendly variant exist. These variants are based on the **b_spacer_template** body object and adopt common features from it via cloning.

The main differences between the spacer variants are listed below:

| Name    | diff to default spacer                   | picture |
|:---------|:----------------------------------------|---------|
| default | -                                        | ![](../pics/spacer_default.png) |
| qwiic   | bit more space for internal, qwiic-connected addons at the cost of less missalignment protection during assembly| ![](../pics/spacer_qwiic.png) |

**Important:** When applying changes to this template object, make sure the changes actually propagate to all downstream clones by triggering a manual recompute of all involved objects. This can be archived by right clicking objects in the model tree and selecting ```Recompute object```.

### Various Subunits
The Subunits of the badge are located in their own .FCStd files. Filenames should be self-explainatory.

Keep in mind:
* Editing a referenced object from a subunit in the full assembly will result in changes in the subunits file and vice versa.
* Some FreeCAD operations can only be performed with the actual object, not with the link object referencing it.

### Convention
To keep things structured and browsable we adhere to the following conventions:

* Subunit files should be initialized with an assembly4 container
* Only assembly containers from subunits are referenced in the full assembly
* The flow3rs full assembly does not reference individual objects from subunit files

* Dimensions that are of global relevance are stored in the spreadsheet in **master_sketches.FCStd** and should be referenced from there

* We use the following prefixes when naming FreeCAD objects:

  * Parts: p_
  * Body: b_
  * Sketch: sk_
  * When an object is inserted into an assembly4 container, the prefix is removed in the link name
  * Objects that are linked multiple time get a reasonable suffix in each instances link name (ie [ _1, _2, _3], [_A, _B], [_left, _right], ... )

* It is preferred to use Assembly4s tools for mirroring, linear or circular arrays for objects that need to be referenced multiple times if they are placed in a symetric or repeating pattern instead of manually placing multiple separate link objects.
